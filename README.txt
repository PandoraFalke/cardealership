***********************************************************************************
                        What is this sample all about? 
***********************************************************************************

This little project is all about storing and deleting data with MicroStream.
To have a better control and get an idea of how MicroStream works with an application,
I've designed a webapplication with RapidClipse X.

***********************************************************************************
                        What you can do with this?
***********************************************************************************

With this project you can create customers who bought or will buy cars by "your" company.
In the application you have 5 views.
- The Homescreen (just a simple Mainpage to navigate to after login)
- The Customer-Tab (accessible as 'user' and 'admin')
    - New Entry: Here you can register a new customer to your system
    - Show all: Here you can see all registered customers with theyre cars (if they have one)
- The Car-Tab (accessible only as 'admin')
    - New Entry: As Admin you can register a new car into your system which was bought by a customer
    - Show all: Here you can see all registered cars with their owners (customers)

***********************************************************************************
                        How to use everything 
***********************************************************************************

To use the programm correctly you have to have installed an IDE of your choice (Mine was Eclipse or better to say RapidClipse)
and also you need an application-server like Wildfly. You can also use any other as long als you can publish the project to your localhost-adress.

1. Install Wildfly (or your prefered application-server) and Setup your IDE.
2. Clone the project to an local repository
3. Import the project to your IDE and also to the application-server.
4. Start Wildfly and navigate to http://localhost:8080/carDealership/* 
5. You will now see an login screen there are only to ways to login
    5.1 Name: user Password: user (access only to Customer-Tab. The Car-Tab will be visible but won't work)
    5.2 Name: admin Password: admin (access to everything)
6. You'll now be able to use the programm, add new customers (or cars as admin), delete entries and show everything
7. If you logout or completely shutdown Wildfly and restart it, every data chance you've made should be saved. 
    So if you added new customers they are still there. If you delete one, they are still gone.


***********************************************************************************
                        How does it work 
***********************************************************************************

MicroStream is a local database. You don't have to setup any external database on your own and have something running in the cloud or somewhere else to let it work.
You can find the 'storage' of the database onto your pc with the path: C:\user\{your username}\microstream-data\carDealership
If you want to setup everything like the first time, you just need to delete the 'storage' folder and restart the application.
MicroStream will then generate everything after the first press on 'Login' and the storage folder should show up again. 
(yes even if you logged in with the wrong username and password. Thats the way I coded it :D )


***********************************************************************************
                        Links to all the stuff I've used 
***********************************************************************************

Wildfly:        https://www.wildfly.org/downloads/ (I'ved used Wildfy 15 but you can choose whatever you want)
MicroStream:    https://manual.docs.microstream.one/data-store/getting-started
RapidClipse:    https://www.rapidclipse.com/en (I prefer RapidClipse X. It's the newest and the first where you can implement MicroStream with just a few clicks)