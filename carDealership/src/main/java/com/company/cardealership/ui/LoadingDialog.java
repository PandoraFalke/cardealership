
package com.company.cardealership.ui;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.progressbar.ProgressBar;


public class LoadingDialog
{
	private final ProgressBar progressBar = new ProgressBar();
	private final Dialog      dialog      = new Dialog();
	private String            message     = "";
	
	public LoadingDialog(final String message)
	{
		this.message = message;

	}
	
	public void open()
	{
		this.progressBar.setIndeterminate(true);
		this.dialog.add("Please wait. " + this.message);
		this.dialog.add(this.progressBar);
		this.dialog.open();
	}
	
	public void close()
	{
		this.progressBar.setIndeterminate(false);
		this.dialog.close();
	}
}
