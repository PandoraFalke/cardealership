
package com.company.cardealership.microstream.data.entites;

public class Car implements Comparable<Car>
{
	private String   name;
	private String   vehicleID;
	private Customer owner;
	
	public Car()
	{

	}

	public Car(final String name, final String vehicleID, final Customer owner)
	{
		super();
		this.name      = name;
		this.vehicleID = vehicleID;
		this.owner     = owner;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(final String name)
	{
		this.name = name;
	}
	
	public String getVehicleID()
	{
		return this.vehicleID;
	}
	
	public void setVehicleID(final String vehicleID)
	{
		this.vehicleID = vehicleID;
	}
	
	public Customer getOwner()
	{
		return this.owner;
	}
	
	public void setOwner(final Customer owner)
	{
		this.owner = owner;
	}

	@Override
	public int compareTo(final Car o)
	{
		return this.name.compareTo(o.name);
	}
	
	@Override
	public String toString()
	{
		return this.name + " " + this.vehicleID;
	}
}
