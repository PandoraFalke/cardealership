
package com.company.cardealership.microstream.data.entites;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Customer implements Comparable<Customer>
{
	private String    forename;
	private String    lastName;
	private List<Car> cars;

	public Customer()
	{

	}
	
	public Customer(final String forename, final String lastName)
	{
		super();
		this.forename = forename;
		this.lastName = lastName;
	}

	public Customer(final String forename, final String lastName, final List<Car> cars)
	{
		super();
		this.forename = forename;
		this.lastName = lastName;
		this.cars     = cars;
	}
	
	public String getForename()
	{
		return this.forename;
	}
	
	public void setForename(final String forename)
	{
		this.forename = forename;
	}
	
	public String getLastName()
	{
		return this.lastName;
	}
	
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}
	
	public List<Car> getCars()
	{
		return this.cars == null ? new ArrayList<>() : this.cars;
	}
	
	public void addCar(final Car car)
	{
		if(this.cars == null)
		{
			this.cars = new ArrayList<>();
		}
		this.cars.add(car);
	}
	
	public void setCars(final List<Car> cars)
	{
		this.cars = cars;
	}

	@Override
	public int compareTo(final Customer o)
	{
		return this.lastName.compareTo(o.lastName);
	}
	
	@Override
	public int hashCode()
	{
		return Objects.hash(this.forename, this.lastName);
	}
	
	@Override
	public boolean equals(final Object obj)
	{
		if(this == obj)
		{
			return true;
		}
		if(!(obj instanceof Customer))
		{
			return false;
		}
		final Customer other = (Customer)obj;
		return Objects.equals(this.forename, other.forename) && Objects.equals(this.lastName, other.lastName);
	}

	@Override
	public String toString()
	{
		return this.forename + " " + this.lastName;
	}
	
}
