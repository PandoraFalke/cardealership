
package com.company.cardealership.microstream.data.service;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinSession;


public class Logout
{
	public static void logout()
	{
		UI.getCurrent().getPage().setLocation("*");
		VaadinSession.getCurrent().getSession().invalidate();
		VaadinSession.getCurrent().close();
	}
}
