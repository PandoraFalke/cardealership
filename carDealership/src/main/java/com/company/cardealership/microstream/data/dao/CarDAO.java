
package com.company.cardealership.microstream.data.dao;

import java.util.List;
import java.util.Set;

import com.company.cardealership.microstream.data.entites.Car;
import com.company.cardealership.microstream.main.MicroStream;


/**
 * You can also use the corresponding MicroStream methods.<br>
 * But with this DAO it becomes more clearer what we want to do inside the code.
 *
 */
public class CarDAO
{
	private CarDAO()
	{
		throw new IllegalStateException("Utility class");
	}
	
	public static Set<Car> findAll()
	{
		return MicroStream.root().getCars();
	}

	public static List<Car> findAllSorted()
	{
		return MicroStream.root().getCarsSorted();
	}

	public static void insert(final Car car)
	{
		CarDAO.findAll().add(car);
		MicroStream.storageManager().store(CarDAO.findAll());
	}

	public static void update(final Car car)
	{
		MicroStream.storageManager().store(car);
	}

	public static void updateAll(final Set<Car> cars)
	{
		MicroStream.storageManager().store(cars);
	}
}
