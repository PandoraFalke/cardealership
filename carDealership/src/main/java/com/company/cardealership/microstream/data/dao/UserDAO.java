
package com.company.cardealership.microstream.data.dao;

import java.util.Set;

import com.company.cardealership.microstream.data.entites.User;
import com.company.cardealership.microstream.main.MicroStream;


/**
 * You can also use the corresponding MicroStream methods.<br>
 * But with this DAO it becomes more clearer what we want to do inside the code.
 *
 */
public class UserDAO
{
	public static Set<User> findAll()
	{
		return MicroStream.root().getUsers();
	}
	
	public static void insert(final User user)
	{
		UserDAO.findAll().add(user);
		MicroStream.storageManager().store(UserDAO.findAll());
	}
	
	public static void update(final User user)
	{
		MicroStream.storageManager().store(user);
	}

	public static void updateAll(final Set<User> users)
	{
		MicroStream.storageManager().store(users);
	}
}
