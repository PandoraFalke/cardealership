
package com.company.cardealership.microstream.data.service;

import com.company.cardealership.microstream.data.entites.Role;
import com.company.cardealership.microstream.data.entites.User;
import com.company.cardealership.ui.MainContainer;
import com.company.cardealership.ui.MainView;
import com.company.cardealership.ui.NewEntryCar;
import com.company.cardealership.ui.NewEntryCustomer;
import com.company.cardealership.ui.ShowAllCar;
import com.company.cardealership.ui.ShowAllCustomer;
import com.vaadin.flow.router.RouteConfiguration;


public class AuthService
{

	public static boolean authUser(final User user, final User loginUser)
	{
		
		if(user.equals(loginUser))
		{
			// creating routes at runtime
			if(loginUser.getUsername().equalsIgnoreCase("admin"))
			{
				loginUser.setRole(Role.ADMIN);
			}
			else if(loginUser.getUsername().equalsIgnoreCase("user"))
			{
				loginUser.setRole(Role.USER);
			}
			
			AuthService.createRoutes(loginUser.getRole());

			return true;
		}
		else
		{
			return false;
		}

	}

	@SuppressWarnings("unchecked")
	private static void createRoutes(final Role role)
	{
		
		RouteConfiguration.forSessionScope().setRoute("home", MainView.class, MainContainer.class);
		RouteConfiguration.forSessionScope().setRoute("customerNewEntry", NewEntryCustomer.class,
			MainContainer.class);
		RouteConfiguration.forSessionScope().setRoute("customerShowAll", ShowAllCustomer.class,
			MainContainer.class);
		if(role.equals(Role.ADMIN))
		{
			RouteConfiguration.forSessionScope().setRoute("carNewEntry", NewEntryCar.class,
				MainContainer.class);
			RouteConfiguration.forSessionScope().setRoute("carShowAll", ShowAllCar.class,
				MainContainer.class);
		}

	}

}
