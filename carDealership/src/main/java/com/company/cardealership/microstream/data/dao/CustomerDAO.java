
package com.company.cardealership.microstream.data.dao;

import java.util.List;
import java.util.Set;

import com.company.cardealership.microstream.data.entites.Customer;
import com.company.cardealership.microstream.main.MicroStream;


/**
 * You can also use the corresponding MicroStream methods.<br>
 * But with this DAO it becomes more clearer what we want to do inside the code.
 *
 */
public class CustomerDAO
{
	private CustomerDAO()
	{
		throw new IllegalStateException("Utility class");
	}

	public static Set<Customer> findAll()
	{
		return MicroStream.root().getCustomers();
	}
	
	public static List<Customer> findAllSorted()
	{
		return MicroStream.root().getCustomersSorted();
	}
	
	public static void insert(final Customer customer)
	{
		CustomerDAO.findAll().add(customer);
		MicroStream.storageManager().store(CustomerDAO.findAll());
	}
	
	public static void update(final Customer customer)
	{
		MicroStream.storageManager().store(customer);
	}
	
	public static void updateAll(final Set<Customer> customers)
	{
		MicroStream.storageManager().store(customers);
	}
	
}
