
package com.company.cardealership.microstream.main;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.company.cardealership.microstream.data.entites.Car;
import com.company.cardealership.microstream.data.entites.Customer;
import com.company.cardealership.microstream.data.entites.User;


/*
 * MicroStream data root.Create your data model from here.
 *
 * @see <a href="https://manual.docs.microstream.one/">Reference Manual</a>
 */
public class DataRoot
{
	private final Set<Customer> customers = new HashSet<>();
	private Set<Car>            cars      = new HashSet<>();
	private Set<User>           users     = new HashSet<>();
	
	public Set<Customer> getCustomers()
	{
		return this.customers;
	}
	
	public List<Customer> getCustomersSorted()
	{
		return this.customers.stream().sorted().collect(Collectors.toList());
	}
	
	public Set<Car> getCars()
	{
		return this.cars;
	}
	
	public List<Car> getCarsSorted()
	{
		return this.cars.stream().sorted().collect(Collectors.toList());
	}

	public void setCars(final Set<Car> cars)
	{
		this.cars = cars;
	}

	public Set<User> getUsers()
	{
		return this.users;
	}
	
	public void setUsers(final Set<User> users)
	{
		this.users = users;
	}
	
}
