
package com.company.cardealership.microstream.data.entites;

import java.util.Objects;

import javax.persistence.Entity;

import org.apache.commons.codec.digest.DigestUtils;


@Entity
public class User
{

	public static User user  = new User("user", "user", Role.USER);
	public static User admin = new User("admin", "admin", Role.ADMIN);
	
	private String username;
	private String password;
	private Role   role;

	public User()
	{
	}

	public User(final String username, final String password, final Role role)
	{
		this.username = username;
		this.role     = role;
		this.password = password;
		
	}

	public boolean checkPassword(final String password)
	{
		return DigestUtils.sha1Hex(password).equals(this.password);
	}

	public String getUsername()
	{
		return this.username;
	}

	public void setUsername(final String username)
	{
		this.username = username;
	}
	
	public String getPassword()
	{
		return this.password;
	}

	public void setPassword(final String passwordHash)
	{
		this.password = passwordHash;
	}

	public Role getRole()
	{
		return this.role;
	}

	public void setRole(final Role role)
	{
		this.role = role;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(this.password, this.username);
	}

	@Override
	public boolean equals(final Object obj)
	{
		if(this == obj)
		{
			return true;
		}
		if(!(obj instanceof User))
		{
			return false;
		}
		final User other = (User)obj;
		return Objects.equals(this.password, other.password) && Objects.equals(this.username, other.username);
	}

	@Override
	public String toString()
	{
		return "User [username=" + this.username + ", role=" + this.role + "]";
	}
	
}
