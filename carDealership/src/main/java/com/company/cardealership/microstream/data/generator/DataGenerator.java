
package com.company.cardealership.microstream.data.generator;

import java.util.Set;

import com.company.cardealership.microstream.data.dao.CarDAO;
import com.company.cardealership.microstream.data.dao.CustomerDAO;
import com.company.cardealership.microstream.data.dao.UserDAO;
import com.company.cardealership.microstream.data.entites.Car;
import com.company.cardealership.microstream.data.entites.Customer;
import com.company.cardealership.microstream.data.entites.User;


public class DataGenerator
{
	
	public static void addUsers()
	{
		
		final Set<User> dbUsers = UserDAO.findAll();
		
		// You might think: Even if the Users are already inside the dbUsers they will be added.
		// ...Not quite! Set<> allows only one specific copy of an object.
		// Therefore the Users won't be added if they already inside the dbUsers.
		dbUsers.add(User.user);
		dbUsers.add(User.admin);
		
	}

	public static void generateSomeDataAndSaveIt()
	{
		final Set<Customer> dbCustomers = CustomerDAO.findAll();
		final Set<Car>      dbCars      = CarDAO.findAll();
		
		if(dbCustomers.isEmpty() && dbCars.isEmpty())
		{
			final Customer john    = new Customer("John", "WithCars");
			final Car      dodge   = new Car("Dodge Charger", "s0m31D", john);
			final Car      touareg = new Car("VW Touareg", "N1c3ID", john);
			
			dbCars.add(dodge);
			dbCars.add(touareg);
			
			john.addCar(dodge);
			john.addCar(touareg);
			
			dbCustomers.add(john);
			dbCustomers.add(new Customer("Dan", "WithoutCar"));

			CustomerDAO.updateAll(dbCustomers);
			CarDAO.updateAll(dbCars);
		}

	}
}
