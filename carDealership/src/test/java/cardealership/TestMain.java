
package cardealership;

import java.util.Set;

import org.junit.jupiter.api.Test;

import com.company.cardealership.microstream.data.entites.User;
import com.company.cardealership.microstream.data.generator.DataGenerator;
import com.company.cardealership.microstream.main.MicroStream;


class TestMain
{
	
	@Test
	void test()
	{
		final Set<User> users = MicroStream.root().getUsers();
		System.out.println(users);
		for(int i = 0; i < 10; i++)
		{
			DataGenerator.addUsers();
			System.out.println("Run: " + i + "\n" + users);
		}
		MicroStream.storageManager().store(users);
		
	}
	
}
